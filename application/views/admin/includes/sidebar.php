<ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('admin/Dashboard'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
 
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('admin/Manage_Users'); ?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span></a>
        </li>
		 <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user/Login/logout'); ?>">
      <i class="fas fa-sign-out-alt"></i>
            <span>Log Out</span></a>
        </li>
 
      </ul>
