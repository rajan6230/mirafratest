<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User dashboard</title>
	<!-- Bootstrap core CSS-->
	<?php echo link_tag('assests/vendor/bootstrap/css/bootstrap.min.css'); ?>
	<!-- Custom fonts for this template-->
	<?php echo link_tag('assests/vendor/fontawesome-free/css/all.min.css'); ?>
	<!-- Page level plugin CSS-->
	<?php echo link_tag('assests/vendor/datatables/dataTables.bootstrap4.css'); ?>
	<!-- Custom styles for this template-->
	<?php echo link_tag('assests/css/sb-admin.css'); ?>

  </head>

  <body id="page-top">

 <?php include APPPATH.'views/user/includes/header.php';?>

    <div id="wrapper">

      <!-- Sidebar -->
 <?php include APPPATH.'views/user/includes/sidebar.php';?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo site_url('user/Dashboard'); ?>">User</a>
            </li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>

          <!-- Icon Cards-->
          <div class="row">
            <div class="col-xl-12 col-sm-6 mb-3">
   <h3>Welcome Back : <?php echo $profile->firstName;?> <?php echo $profile->lastName;?>  </h3>
            </div>
			
			
			<div class="content">
  <div class="container-fluid">
    <h2>Currency Converter Using PHP</h2>   
    <br><br>  
	<?php if ($this->session->flashdata('success')) { ?>
<p style="color:green; font-size:18px;"><?php echo $this->session->flashdata('success'); ?></p>
</div>
<?php } ?>
    <div class="row">

      <form class="form-inline" >
        <div class="form-group classWithPad">
          <label for="Name2">From Currency &nbsp;&nbsp;</label>
          <select class="form-control" id="from_currency_id" required name="from_currency" style="width:170px;">
            <?php  
			$this->load->database();

              $query = $this->db->query("SELECT currency_id,currency_name FROM currency_list");
             
				if ($query->num_rows() > 0)
				{
				   foreach ($query->result() as $currency_list)
				   {
				
            ?>
              <option value="<?php echo $currency_list->currency_id;?>" <?php if($currency_list->currency_id == "USD"){echo 'selected';}?> ><?php echo $currency_list->currency_name; ?></option>
            <?php    } } ?>
          </select>
        </div>
 
        <div class="form-group classWithPad">
          <label for="Email2">Amount&nbsp;&nbsp;</label>
          <input style="width:90px;" type="text" class="form-control" name="amount" id="amount_id" required>
        </div>
 
        <div class="form-group classWithPad">
          <label for="Email2">To Currency &nbsp;&nbsp;</label>
          <select class="form-control" id="to_currency_id" name="to_currency" required style="width:170px;">
           <?php  
              $sql_query  = $this->db->query("SELECT currency_id,currency_name FROM currency_list");
              if ($sql_query->num_rows() > 0)
				{
				   foreach ($sql_query->result() as $currency_list)
				   {
            ?>
              <option value="<?php echo $currency_list->currency_id;?>" <?php if($currency_list->currency_id == "INR"){echo 'selected';}?>><?php echo $currency_list->currency_name; ?></option>
            <?php    } } ?>
          </select>
        </div>
 
         <div class="form-group classWithPad">
          <label for="Email2">Converted Amount&nbsp;</label>
          <input style="width:90px;" type="text" class="form-control" id="converted_amount_id" readonly="">
        </div>
 
        <div class="form-group classWithPad">
          <button type="button"  onclick="convertC();" class="btn btn-danger">Save</button>
        </div>
      </form>
    </div>

  </div>  
</div>
  
          </div>



        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
   <?php include APPPATH.'views/user/includes/footer.php';?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->


    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo base_url('assests/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?php echo base_url('assests/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>

    <!-- Page level plugin JavaScript-->
    <script src="<?php echo base_url('assests/vendor/chart.js/Chart.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/vendor/datatables/jquery.dataTables.js'); ?>"></script>
    <script src="<?php echo base_url('assests/vendor/datatables/dataTables.bootstrap4.js'); ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url('assests/js/sb-admin.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/demo/datatables-demo.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/demo/chart-area-demo.js'); ?>"></script>
	
<script type="text/javascript">

function convertC()
{
  var from_currency =   $('#from_currency_id').val(); 
  
  var to_currency   =   $('#to_currency_id').val();     
  var amount        =   $('#amount_id').val();
  $.post("<?php echo site_url('/user/dashboard')?>",
    {
		type:"POST",
      from_currency: from_currency,
      to_currency: to_currency,
      amount:amount,
	   data : {from_currency:from_currency , to_currency:to_currency, amount:amount}
	    //alert("uuu");
    },
    function(data, status)
    {
      if(status == 'success')
      { 
        var data = JSON.parse(data);
		alert(data);
        console.log(data.error);
        if(data.error == 0 && data.message == 'ok')
        {
          var converted_amount = data.converted_result.replace(/^"(.*)"$/, '$1');  
          $('#converted_amount_id').val(converted_amount);
        }
        else
        {
          alert(data.message);
          return;
        }
      }
      else
      {
        alert('Please try again!');
      }
    }
  ); 
}
</script>
  </body>

</html>
