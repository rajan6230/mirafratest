<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Dashboard extends CI_Controller {
function __construct(){
parent::__construct();
if(! $this->session->userdata('uid'))
redirect('user/login');
}
public function index(){
	$this->load->model('Currency_Converter_Model');
	$this->load->database();

		if($this->input->post('from_currency') && $this->input->post('to_currency') && $this->input->post('amount'))
		{
		 $from_currency 	= 	$this->input->post('from_currency');
		$to_currency 	= 	$this->input->post('to_currency');
		$amount 	=       $this->input->post('amount');		
		$converted_result=$this->Currency_Converter_Model->save_currancy();
		$this->session->set_flashdata('success','Profile updated successfull.');
		return redirect('/index');
		}
	
	
	$userid = $this->session->userdata('uid');
	$this->load->model('User_Profile_Model');
	$profiledetails=$this->User_Profile_Model->getprofile($userid);
	$this->load->view('user/dashboard',['profile'=>$profiledetails]);

}
public function convertCurrency($amount , $from_currency , $to_currency ){
	
  $from_Currency = urlencode($from_currency);
  $to_Currency = urlencode($to_currency);
  $query =  "{$from_Currency}_{$to_Currency}";
 
  $json = file_get_contents("http://free.currencyconverterapi.com/api/v6/convert?q={$query}&amp;compact=ultra&amp;apiKey=your_api_key");
  $obj = json_decode($json, true);
 
  $val = floatval($obj["$query"]);
 
 
  $total = $val * $amount;
  return number_format($total, 2, '.', '');
}
}
