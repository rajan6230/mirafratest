<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Currency_Converter_Model extends CI_Model {

	function save_currancy(){
        $data = array(
                
				'from_currency' => 	$this->input->post('from_currency'),
				'to_currency' 	=> 	$this->input->post('to_currency'),
				'amount' 	=>       $this->input->post('amount')
            );
        $result=$this->db->insert('currency_convertar',$data);
        return $result;
    }
}
